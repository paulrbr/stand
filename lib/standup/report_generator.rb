require 'gitlab'
require 'terminal-table'

module Standup
  class ReportGenerator
    def initialize(settings)
      @settings = settings
      @result = {
        'done'     => [],
        'reviewed' => [],
      }

      configure_gitlab_client
    end

    def gitlab_report!
      projects_query = {
        per_page: 10,
        scope: :owned,
        search: config['gitlab_project_search_pattern'],
        order_by: :last_activity_at,
        sort: :desc
      }

      mrs_query = {
        per_page: 100,
        state: :merged,
        order_by: :updated_at,
        sort: :desc
      }

      issues_query = {
        per_page: 100,
        state: :closed,
        order_by: :updated_at,
        sort: :desc
      }

      projects = Gitlab.projects(projects_query)

      projects.each do |project|
        begin
          merged_mrs = Gitlab.merge_requests(project.id, mrs_query)
        rescue Gitlab::Error::Forbidden
          # Don't seem to have acces to some MRs
          merged_mrs = []
        end

        begin
          closed_issues = Gitlab.issues(project.id, issues_query)
        rescue Gitlab::Error::Forbidden
          closed_issues = []
        end

        merged_mrs.each do |mr|
          validate_state(project, mr, 'MR')
        end

        closed_issues.each do |issue|
          validate_state(project, issue, 'ISSUE')
        end
      end

      puts Terminal::Table.new :title => 'DONE', :headings => [ 'PROJECT', 'TYPE', 'ID', 'TITLE' ], :rows => @result['done']
      puts "\n"
      puts Terminal::Table.new :title => 'REVIEWED', :headings => [ 'PROJECT', 'TYPE', 'ID', 'TITLE' ], :rows => @result['reviewed']

    end

    private

    def config
      @settings.config
    end

    def to_date
      @settings.to_date
    end

    def from_date
      @settings.from_date
    end

    def validate_state(project, event, type='?')
      updated_at = Time.parse(event.updated_at)
      author     = event.author.username
      assignee   = event.assignee && event.assignee.username

      if updated_at < to_date && updated_at > from_date
        if author == config[:username]
          @result['done'] << [project.name, type, "##{event.iid}", event.title]
        elsif assignee == config[:username]
          @result['reviewed'] << [project.name, type, "##{event.iid}", event.title]
        end
      end
    end

    def configure_gitlab_client
      api_endpoint = config['gitlab_endpoint'] || 'https://gitlab.com/api/v3'.freeze

      Gitlab.configure do |c|
        c.endpoint = api_endpoint
        c.private_token = config['gitlab_access_token']
      end
    end
  end
end
